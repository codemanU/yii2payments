<?php

use yii\grid\GridView;
/* @var $this yii\web\View */

$this->title = 'Payments';
?>
<div class="site-index">
    <h2>All users</h2>
    <div class="body-content">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'username',
                'created_at:datetime',
                'balance'
            ],
        ]) ?>
    </div>
</div>
