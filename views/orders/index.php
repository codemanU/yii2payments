<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payments';
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Payment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'created_at:date',
            [
                'attribute'=>'from_user_id',
                'label'=>'From user id',
                'format'=>'text',
                'content'=>function($data){
                    $user = User::findOne($data->from_user_id);
                    return $user->username;
                },
            ],
            [
                'attribute'=>'to_user_id',
                'label'=>'To user id',
                'format'=>'text',
                'content'=>function($data){
                    $user = User::findOne($data->to_user_id);
                    return $user->username;
                },
            ],
            [
                'attribute'=>'order_type',
                'label'=>'Order type',
                'format'=>'text',
                'content'=>function($data){
                    return $data->order_type ? "Pay" : "Bill";
                },
            ],
            'value',
            [
                'attribute'=>'is_processed',
                'label'=>'Is processed',
                'format'=>'text',
                'content'=>function($data){
                    return $data->is_processed ? "Yes" : "No";
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
