<?php

use yii\db\Migration;

/**
 * Handles the creation for table `orders_table`.
 */
class m160620_103845_create_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'to_user_id' => $this->integer()->notNull(),
            'from_user_id' => $this->integer()->notNull(),
            'order_type' => $this->smallInteger(),
            'value' => $this->float()->notNull(),
            'is_processed' => $this->boolean()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('orders');
    }
}
