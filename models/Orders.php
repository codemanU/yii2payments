<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;


/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 * @property integer $order_type
 * @property double $value
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_user_id', 'to_user_id', 'value', 'order_type'], 'required'],
            [['from_user_id', 'order_type'], 'integer'],
            [['value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'from_user_id' => 'From User',
            'to_user_id' => 'To User',
            'order_type' => 'Order Type',
            'value' => 'Value',
        ];
    }

    public function process($username, $value, $type)
    {
        if ($type == 0) {
            $this->bill($username, $value);
        }
        if ($type == 1) {
            $this->pay($username, $value);
        }
    }

    public function bill($username, $value)
    {
        $to_user = $this->findOrCreateUser($username);
        $this->to_user_id = $to_user->id;
        $this->value = $value;
    }

    public function pay($username, $value)
    {
        $current_user = \Yii::$app->user->identity;
        $bills = Orders::find()
            ->where(['to_user_id' => \Yii::$app->user->identity->id])
            ->andWhere(['order_type' => 0])
            ->orderBy('created_at')
            ->all();
        if ($bills) {
            foreach ($bills as $bill) {
                $process_bill = Orders::findOne($bill->id);
                $current_user->balance -= $process_bill->value;
                if ($process_bill->value <= $value) {
                    $process_bill->is_processed = true;
                    $process_bill->save(false);

                    $to_user = $this->findOrCreateUser($username);
                    $current_user->balance -= $value;
                    $to_user->balance += $value;
                    $to_user->save(false);
                    $current_user->save(false);
                }
            }
            return;
        }else{
            $to_user = $this->findOrCreateUser($username);
            $current_user->balance -= $value;
            $to_user->balance += $value;
            $to_user->save(false);
            $current_user->save(false);
            $this->is_processed = true;
        }


    }

    public function findOrCreateUser($username)
    {
        $to_user = User::findOne(['username' => $username]);
        if ($to_user) {
            $this->to_user_id = $to_user->id;
            return $to_user;
        }else{
            $user = new User();
            $user->username = $username;
            $user->setPassword('123456789');
            $user->save(false);
            return $user;
        }
    }
}
